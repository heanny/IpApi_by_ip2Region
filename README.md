# IpApi_by_ip2Region

#### 介绍

1. 自用
2. 基于https://gitee.com/lionsoul/ip2region 的ip定位查询
3. 接口栗子：http://www.heanny.cn/ip2region_10.24.24.22.js  
 （http://www.heanny.cn/ip2region_{ip}.js）


准确率99.9%的ip地址定位库，0.0x毫秒级查询，数据库文件大小只有1.5M，提供了java,php,c,python,nodejs,golang,c#查询绑定和Binary,B树,内存三种查询算法，妈妈再也不用担心我的ip地址定位！



#### 安装教程

1. 下载之环境即可
2. 更多使用详情，请阅读原作者git


#### 使用说明

1. Ip2RegionApi.php/Ip2RegionApi.py为自用文件
2. python版为查询list方法
3. php版为查询单个ip方法



### 注意事项
原作者不定期更新ip数据，请移步至原作者查看及更新数据