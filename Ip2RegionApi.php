<?php
/**
 * Ip2Region php client API
 * @author    Heanny<lzh@heanny.cn>
 */
header("Content-Type: text/html; charset=utf8");
function GetIpRegion($ip,$algorithm='B-tree',$method = 'btreeSearch'){
    $dbFile = './data/ip2region.db';
    switch ($algorithm) {
        case 'binary':
            $algorithm = 'Binary';
            $method = 'binarySearch';
            break;
        case 'memory':
            $algorithm = 'Memory';
            $method = 'memorySearch';
            break;
    }
    require dirname(__FILE__) . '/php/Ip2Region.class.php';
    $ip2regionObj = new Ip2Region($dbFile);
    if (strlen($ip) < 2 || $ip == 'quit') {
        return null;
    };
    $s_time = getTime();
    $data = $ip2regionObj->{$method}($ip);
    $c_time = getTime() - $s_time;
    $data['time']=$c_time;
    $data['ip']=$ip;
//    printf("%s|%s in %.5f ms\n", $data['city_id'], $data['region'], $c_time);
//    print_r($data);
    return $data;

}

function getTime()
{
    return (microtime(true) * 1000);
}
$ip=$_REQUEST['ip'];
print_r( GetIpRegion($ip));
?>
